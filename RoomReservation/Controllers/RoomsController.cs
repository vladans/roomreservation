﻿using Microsoft.Ajax.Utilities;
using RoomReservation.Models;
using RoomReservation.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace RoomReservation.Controllers
{
    public class RoomsController : Controller
    {
        private RoomReservationContext db = new RoomReservationContext();
        List<string> SortTypes = new List<string> { "Beds number", "Beds number descending", "Price", "Price Descending" };
        private int itemsPerPage = 2;

        // GET: Rooms
        public ActionResult Index(/*string search,*/ string sortType = "Beds number", int page = 1)
        {
            var rooms = db.Rooms.Include(r => r.Reservations);

            //if (!search.IsNullOrWhiteSpace())
            //{
            //    rooms = rooms.Where(
            //        x => x.Name.Contains(search) ||
            //        x.Accommodation.Name.Contains(search)
            //    );
            //}

            switch (sortType)
            {
                case "Beds number":
                    rooms = rooms.OrderBy(x => x.BedNumber);
                    break;
                case "Beds number descending":
                    rooms = rooms.OrderByDescending(x => x.BedNumber);
                    break;
                case "Price":
                    rooms = rooms.OrderBy(x => x.Price);
                    break;
                case "Price Descending":
                    rooms = rooms.OrderByDescending(x => x.Price);
                    break;
            }

            var model = new RoomAccommodationsViewModel();
            model.SortTypes = SortTypes;
            model.Rooms = rooms.ToPagedList(page, itemsPerPage);
            return View(model);
        }

        // GET: Rooms/Create
        public ActionResult Create()
        {
            var model = new RoomAccommodationsViewModel();
            model.Accommodations = db.Acommodations.ToList();
            return View(model);
        }

        // POST: Rooms/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoomAccommodationsViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Room.AccommodationId = model.selectedAccommodation;
                db.Rooms.Add(model.Room);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Rooms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var room = db.Rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            var model = new RoomAccommodationsViewModel();
            model.Accommodations = db.Acommodations.ToList();
            model.Room = room;
            model.selectedAccommodation = room.AccommodationId;
            return View(model);
        }

        // POST: Rooms/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoomAccommodationsViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Room.AccommodationId = model.selectedAccommodation;
                db.Entry(model.Room).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Filter(RoomAccommodationsViewModel vm, int page = 1)
        {
            var rooms = db.Rooms
                .Include(x => x.Accommodation)
                .Include(x => x.Reservations);

            if (vm.RoomFilter.BedNumberFrom != null)
            {
                rooms = rooms.Where(x => x.BedNumber >= vm.RoomFilter.BedNumberFrom)
                    .OrderBy(x => x.Id);

                //Model.Name = db.Products.Where(p => p.ProductSubcategoryID == id)
                //        .OrderBy(p => p.ProductSubcategoryID) // <---- this
                //        .Skip((page - 1) * pageSize)
                //        .Take(/*pageSize*/ itemsPerPage)
                //        .ToList();
            }

            if (vm.RoomFilter.BedNumberTo != null)
            {
                rooms = rooms.Where(x => x.BedNumber <= vm.RoomFilter.BedNumberTo)
                    .OrderBy(x => x.Id); ;
            }

            if (vm.RoomFilter.PriceFrom != null)
            {
                rooms = rooms.Where(x => x.Price >= vm.RoomFilter.PriceFrom);
            }

            if (vm.RoomFilter.PriceTo != null)
            {
                rooms = rooms.Where(x => x.Price <= vm.RoomFilter.PriceTo);
            }

            if (!vm.RoomFilter.AccommodationName.IsNullOrWhiteSpace())
            {
                rooms = rooms.Where(x => x.Accommodation.Name.Contains(vm.RoomFilter.AccommodationName));
            }

            //if (filter.CreatedFrom != null)
            //{
            //    products = products.Where(p => p.Created >= filter.CreatedFrom);
            //}

            //if (filter.CreatedTo != null)
            //{
            //    products = products.Where(p => p.Created <= filter.CreatedTo);
            //}

            //var model = new RoomAccommodationsViewModel();
            //model.SortTypes = SortTypes;
            vm.Rooms = rooms.ToPagedList(page, itemsPerPage);
            return View(vm);
        }

        // POST: Rooms/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var room = db.Rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            db.Rooms.Remove(room);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}