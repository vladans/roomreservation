﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoomReservation.Models
{
    public class RoomFilter
    {
        [Display(Name = "Bed Number From")]
        public int? BedNumberFrom { get; set; }
        [Display(Name = "Bed Number To")]
        public int? BedNumberTo { get; set; }
        [Display(Name = "Price From")]
        public decimal? PriceFrom { get; set; }
        [Display(Name = "Price To")]
        public decimal? PriceTo { get; set; }
        [Display(Name = "Accommodation")]
        public string AccommodationName { get; set; }
    }
}