﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoomReservation.Models
{
    // Soba ima sledeće atribute: Id, Broj sobe, Broj kreveta, Cena po noćenju
    public class Room
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Room Number")]
        public int RoomNumber { get; set; }

        [Required]
        [Display(Name = "Bed Number")]
        public int BedNumber { get; set; }

        [Required]
        public decimal Price { get; set; }

        public virtual Accommodation Accommodation { get; set; }
        public int? AccommodationId { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; }

    }
}