﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RoomReservation.Models
{
    public class RoomReservationContext : DbContext
    {
        public RoomReservationContext() : base("name=RoomReservationConnection")
        {
        }

        public DbSet<Room> Rooms { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Accommodation> Acommodations { get; set; }
        
    }
}