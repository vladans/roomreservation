﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoomReservation.Models
{
    // Smeštaj ima sledeće atribute: Id, Naziv, Opis, Adresa, Ocena
    public class Accommodation
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public string Adress { get; set; }

        public int Grade { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }

    }
}