﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoomReservation.Models
{
    // Rezervacija ima sledeće atribute: Id, Ime i prezime korisnika(ko rezerviše sobu), Datum od(kad počinje boravak u sobi),
    // Datum do (kad se završava boravak u sobi), Otkazana(svaka rezervacija inicijalno ima vrednost false, ne unosi korisnik kroz formu!)
    public class Reservation
    {
        public int Id { get; set; }

        [Required]
        public string GuestName { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public bool Canceled { get; set; }

        public virtual Room Room { get; set; }
        public int? RoomId { get; set; }

    }
}