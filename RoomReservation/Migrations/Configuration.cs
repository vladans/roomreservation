namespace RoomReservation.Migrations
{
    using RoomReservation.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RoomReservation.Models.RoomReservationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RoomReservation.Models.RoomReservationContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Acommodations.AddOrUpdate(a => a.Id, new Accommodation() { Id = 1, Name = "Hotel Owl", Description = "Night hotel", Adress = "California", Grade = 8 });
            context.Acommodations.AddOrUpdate(a => a.Id, new Accommodation() { Id = 2, Name = "Motel Chainsaw", Description = "Bloody motel", Adress = "Dark side of the moon", Grade = 10 });
            context.Acommodations.AddOrUpdate(a => a.Id, new Accommodation() { Id = 3, Name = "Camp David", Description = "Just elite", Adress = "american dream", Grade = 5 });

            context.Rooms.AddOrUpdate(r => r.Id, new Room() { Id = 1, RoomNumber = 4, BedNumber = 4, Price = 29.99m, AccommodationId = 1 });
            context.Rooms.AddOrUpdate(r => r.Id, new Room() { Id = 2, RoomNumber = 7, BedNumber = 3, Price = 24.99m, AccommodationId = 2 });
            context.Rooms.AddOrUpdate(r => r.Id, new Room() { Id = 3, RoomNumber = 9, BedNumber = 2, Price = 19.99m, AccommodationId = 3 });

            context.Reservations.AddOrUpdate(r => r.Id, new Reservation() { Id = 1, GuestName = "Nikola Tesla", StartDate = DateTime.Parse("01/11/2018 20:18:04"), EndDate = DateTime.Parse("11/11/2018 20:18:04"), Canceled = false, RoomId = 1 });
            context.Reservations.AddOrUpdate(r => r.Id, new Reservation() { Id = 2, GuestName = "Toma Neverica", StartDate = DateTime.Parse("01/10/2018 20:18:04"), EndDate = DateTime.Parse("26/10/2018 20:18:04"), Canceled = false, RoomId = 2 });
            context.Reservations.AddOrUpdate(r => r.Id, new Reservation() { Id = 3, GuestName = "Baba Roga", StartDate = DateTime.Parse("13/11/2018 20:18:04"), EndDate = DateTime.Parse("18/11/2018 20:18:04"), Canceled = false, RoomId = 3 });
        }
    }
}
