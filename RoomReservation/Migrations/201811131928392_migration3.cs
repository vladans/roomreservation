namespace RoomReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rooms", "AccommodationId", "dbo.Accommodations");
            DropIndex("dbo.Rooms", new[] { "AccommodationId" });
            AlterColumn("dbo.Rooms", "AccommodationId", c => c.Int());
            CreateIndex("dbo.Rooms", "AccommodationId");
            AddForeignKey("dbo.Rooms", "AccommodationId", "dbo.Accommodations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "AccommodationId", "dbo.Accommodations");
            DropIndex("dbo.Rooms", new[] { "AccommodationId" });
            AlterColumn("dbo.Rooms", "AccommodationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Rooms", "AccommodationId");
            AddForeignKey("dbo.Rooms", "AccommodationId", "dbo.Accommodations", "Id", cascadeDelete: false);
        }
    }
}
