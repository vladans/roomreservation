﻿using PagedList;
using RoomReservation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RoomReservation.ViewModels
{
    public class RoomAccommodationsViewModel
    {
        public Room Room { get; set; }
        public IPagedList<Room> Rooms { get; set; }
        public List<Accommodation> Accommodations { get; set; }
        public int? selectedAccommodation { get; set; }
        public List<string> SortTypes { get; set; }
        public string sortType { get; set; }
        public RoomFilter RoomFilter { get; set; }

    }
}